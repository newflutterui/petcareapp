import 'package:flutter/material.dart';

class Notification1 extends StatefulWidget {
  const Notification1({super.key});

  @override
  State<Notification1> createState() => _NotificationState();
}

class _NotificationState extends State<Notification1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                const Padding(padding: EdgeInsets.only(left: 20)),
                Container(
                  height: 26,
                  width: 26,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: const Color.fromRGBO(245, 146, 69, 1),
                  ),
                  child: Image.asset('assets/images/image52.png'),
                ),
                const SizedBox(
                  width: 150,
                ),
                const Text(
                  'Notifications',
                  style: TextStyle(height: 16, fontWeight: FontWeight.w500),
                )
              ],
            ),
            const Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 20)),
                Text(
                  'Today',
                  style: TextStyle(height: 14, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            Container(
              height: 104,
              width: 450,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image12.png'),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        'Your checkout is successfull, product is on \ntne way',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image5.png'),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'Appointment request accepted',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                ],
              ),
            ),
            const Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 20)),
                Text(
                  '25 September',
                  style: TextStyle(height: 14, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            Container(
              height: 168,
              width: 450,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image12.png'),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        'Your checkout is successfull, product is on \ntne way',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image5.png'),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'Appointment request accepted',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image6.png'),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'Vaccinate your pet timely',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                ],
              ),
            ),
            const Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 20)),
                Text(
                  '15 September',
                  style: TextStyle(height: 14, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            Container(
              height: 168,
              width: 450,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image12.png'),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        'Your checkout is successfull, product is on \ntne way',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image5.png'),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'Appointment request accepted',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image6.png'),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'Vaccinate your pet timely',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                ],
              ),
            ),
            const Row(
              children: [
                Padding(padding: EdgeInsets.only(left: 20)),
                Text(
                  '21 October',
                  style: TextStyle(height: 14, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            Container(
              height: 168,
              width: 450,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                children: [
                  const Padding(padding: EdgeInsets.only(top: 8)),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image12.png'),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        'Your checkout is successfull, product is on \ntne way',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image5.png'),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'Appointment request accepted',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      const Padding(padding: EdgeInsets.only(left: 10)),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: const Color.fromRGBO(252, 219, 193, 1),
                        ),
                        child: Image.asset('assets/images/image6.png'),
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      const Text(
                        'Vaccinate your pet timely',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
