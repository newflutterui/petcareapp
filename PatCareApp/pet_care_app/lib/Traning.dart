import 'package:flutter/material.dart';

class Traning extends StatefulWidget {
  const Traning({super.key});

  @override
  State<Traning> createState() => _TraningState();
}

class _TraningState extends State<Traning> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                const Padding(padding: EdgeInsets.only(left: 20)),
                Container(
                  height: 26,
                  width: 26,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: const Color.fromRGBO(245, 146, 69, 1),
                  ),
                  child: Image.asset('assets/images/image52.png'),
                ),
                const SizedBox(
                  width: 150,
                ),
                const Text(
                  'Traning',
                  style: TextStyle(height: 16, fontWeight: FontWeight.w500),
                )
              ],
            ),
            Container(
              height: 222,
              width: 450,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/images/image43.png'),
            ),
            Container(
              height: 222,
              width: 450,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/images/image44.png'),
            ),
            Container(
              height: 222,
              width: 450,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/images/image45.png'),
            ),
            Container(
              height: 222,
              width: 450,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/images/image46.png'),
            ),
            Container(
              height: 222,
              width: 450,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Image.asset('assets/images/image47.png'),
            ),
          ],
        ),
      ),
    );
  }
}
