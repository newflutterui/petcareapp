import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pet_care_app/Dashboard.dart';
import 'package:pet_care_app/DetailDoctor.dart';
import 'package:pet_care_app/Grooming.dart';
import 'package:pet_care_app/Login.dart';
import 'package:pet_care_app/SplashScreen.dart';
import 'package:pet_care_app/Traning.dart';
import 'package:pet_care_app/Notification.dart';
import 'package:pet_care_app/Veterinary.dart';
import 'package:pet_care_app/Shop.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      // home: Login(),
      // home: Dashboard(),
      // home: Traning(),
      // home: Notification1(),
      // home: Veterinary(),
      // home: DetailDoctor(),
      // home: Grooming(),
      // home: Shop(),
    );
  }
}
